## Key Features

An overview of features supported by NuttX in no particular order is the following:

* Standards Compliant.
* Core Task Management.
* Modular design.
* Fully preemptible.
* Naturally scalable.
* Highly configurable.
* Easily extensible to new processor architectures, SoC * architecture, or board architectures. See Porting Guide.
* FIFO, round-robin, and “sporadic” scheduling.
* Realtime, deterministic, with support for priority inheritance.
* Tickless operation.
* POSIX/ANSI-like task controls, named message queues, counting semaphores, clocks/timers, signals, pthreads, cancellation points, environment variables, filesystem.
* VxWorks-like task management and watchdog timers.
* BSD socket interface.
* Extensions to manage pre-emption.
* Optional tasks with address environments (Processes).
* Symmetric Multi-Processing (SMP)
* Loadable kernel modules; lightweight, embedded shared libraries.
* Memory Configurations: (1) Flat embedded build, (2) * Protected build with MPU, and (3) Kernel build with MMU.
* Memory Allocators: (1) standard heap memory allocation, (2) granule allocator, (3) shared memory, and (4) dynamically sized, per-process heaps.
* Thread Local Storage (TLS)
* Inheritable “controlling terminals” and I/O redirection. * Pseudo-terminals.
* On-demand paging.
* System logging
* May be built either as an open, flat embedded RTOS or as a separtely built, secure kernel with a system call gate interface.
* Built-in, per-thread CPU load measurements.
* Custom NuttX C library
* Application interface well documented in the NuttX User Guide.

> **TODO**: add general list of supported architectures