# Building NuttX

NuttX in itself is comprised mainly of a core component, the NuttX source itself, and possibly other components such as external applications, libraries, etc.

The build process encompasses 4 phases:

1. Organizing your NuttX project
2. Configuration of Nuttx to a specific board and build platform
3. Compile and linking the Nuttx binary
4. Flashing of a Nuttx binary to hardware


## Build process

There are two main ways of organizing your NuttX based project: 

* building your code inside NuttX build process 
* first building NuttX as a library and then linking your application against NuttX. 

In the first case, issuing `make` will compile both NuttX and your project code, while in the second, issuing `make export` will generate a library and set of headers which can then be used by you separately. In this case, compiling and linking the final image is up to you.

In either case, building NuttX itself closely resembles the build process of the GNU/Linux kernel, involving first a configuration step (using the known `menuconfig` approach) and then a build step. During configuration, you would choose the target architecture, platform and lots of different options and features that can be turned on or off.

### Inside NuttX tree

NuttX build system is designed for a specific directory layout (although there are slight variants to this) where the core NuttX source directory should be placed alongside any other optional components. For example, when including the optional `apps` component, the directory layout will look like this:

    $ cd my_nuttx_project
    $ ls 
    nuttx  apps
    
This is the typical case where you would place your project code directly inside NuttX tree, adding applications to the `apps` tree and drivers and other architecture code in `nuttx`.

Building the project will involve compiling as:

    $ cd nuttx/
    $ make
    
which will generate the `nuttx` binary.

> **Note**: in this example, NuttX is assumed to be already configured

### NuttX as a library 

However, in some cases it would be desireable to not add any foreign code to these directories and work out of this tree. In this case you would organize the code as in the previous case but you would do:

     $ cd nuttx/
     $ make export
     
 This will generate a `nuttx-export.zip` file which can be decompressed anywhere else to have only the relevant files (headers and libraries) required to build your project.
 
> **Note**: for any OS-level code you will still have to interact with NuttX's own build-system

## Configuring NuttX

Before building NuttX it will have to be configured. The typical scenario, when working with an existing supported board is to used *canned* configurations which can be later customized.

In case you would like to base your project on the STM32F4-Discovery board (which is already supported) you can configure NuttX the following way:

    $ cd nuttx/tools
    $ ./configure.sh stm32f4discovery/nsh
    
This will copy the configuration from `nuttx/configs/smt32f4discovery/nsh` directory into the main `nuttx` directory. Now, you can build the project directly by doing `make` or you can modify the configuration before building by using the `menuconfig` target:

    $ make menuconfig
    
This will open an [interactive configuration tool](https://en.wikipedia.org/wiki/Menuconfig) where you can change different settings as shown below.

![](/assets/menuconfig.png)

The menu can be helpful to review a number of settings, e.g. the build platform or debug output options. After exiting and saving these settings you can build NuttX.

### Per-board Configurations

In the previous example you may have wondered about the `nsh` directory. You will notice that for each supported board there will be a corresponding directory inside the `nuttx/configs` directory. 

Each of these directories holds the logic, configurations and makefile rules require to build NuttX for a given *board*. Furthermore, since a board might be used for different purposes and scenarios, sometimes there will be different NuttX configurations provided for one board. By convention, there should at least be a `nsh` configuration which will build a standard NuttX Shell based variant, which will allow you to interact via a command-line interface typically via a standard serial connection. Other variants may enable or disable devices and applications.

The typical structure of a per-board configuration is a directory (such as `nsh`) inside the corresponding board directory (such as `configs/smt32f4discovery`) which will include a `defconfig` file (the configuration itself) and a `Make.defs` file (the build rules for this board). The `configure.sh` script used above is a convenience script which simply copies these to the appropriate places. The `defconfig` file is copied to `nuttx/.config` and the `Make.defs` file to `nuttx/Make.defs`.

You can always undo this by issuing `make distclean` in the main NuttX directory which will erase these files.

## Other `make` Targets

In order to remove all output generated by the `make` process you can issue a `make clean`. Note that this will not remove the configuration copied by the `configure.sh` script.

If you only wish to clean the output of the `apps` build you can use `make apps_clean`.

Finally, you can usually flash the resulting NuttX image to the board using the `make download` target, although this requires the board code to include the relevant target in the `Make.defs` file. Otherwise, you should flash the image manually using your prefered tool.  

> **TODO**: is there a target to clean all depends? Sometimes these need to be regenerated.
