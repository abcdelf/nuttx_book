# About NuttX

NuttX is a real-time operating system (RTOS) with an emphasis on standards compliance and small footprint. Scalable from 8-bit to 32-bit microcontroller environments, the primary governing standards in NuttX are Posix and ANSI standards. Additional standard APIs from Unix and other common RTOS's (such as VxWorks) are adopted for functionality not available under these standards, or for functionality that is not appropriate for deeply-embedded environments (such as fork()).

NuttX was first released in 2007 by [Gregory Nutt](http://nuttx.org/doku.php?id=wiki:developers:gregory_nutt) under the permissive BSD license.

# About this book
This book aims to provide both a broad view of the NuttX RTOS and also to cover specific topics in detail. It is targeted towards new users who want to start using NuttX. 

## Contributing

The book is an ongoing work and contributions are most welcome. 

This book is maintained in a [GitLab repository](https://gitlab.com/phreakuencies/nuttx_book) and contributions should be made in the form of merge requests so they can be properly reviewed before begin included. 

In case you are not able or interested in writing something, you can also open an issue requesting documentation on areas lacking detail or to suggest new chapters or improvements.

### How to edit this book

In order to edit this book and render it locally on your computer you should first clone the corresponding git repository. Then, you can start editing the files written in Markdown format.

There are many graphical Markdown editors available. One simple editor is [Remarkeable](https://remarkableapp.github.io/). On the other hand, using the [Gitbook Editor](https://www.gitbook.com/editor) is recommended.

There are also official [command-line tools](https://toolchain.gitbook.com/setup.html) which can be used to render this documentation and view the result as it would appear in the final version. To have an always up-to-date view of the book in your browser you should simply do:  

    $ gitbook serve
    
which will start a local web-server which you can access from your browser at `http://localhost:4000`




