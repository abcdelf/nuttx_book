> **TODO** topics to cover:
* architecture vs board chapter
 * difference between architecture specific and board code and how they interact
 * naming conventions of board-specific and architecture-specifc functions
 * from architecture to chip specific code, how are chip specific definitions organized (pin configuration, memory layout, on board resources)
 * board.h and how it interacts with above pin definitions
* other chapters (to be organized)
 * flat vs protected build
 * build toolchain and issues arising from newlib-based toolchains
 