# Boot Sequence 

At the highest level, the NuttX initialization sequence can be represented in three phases:

 1. The hardware-specific power-on reset initialization (initialize memory and run `__start()`, which sets up low-level architecture-specific hardware settings),
 2. NuttX RTOS initialization (`os_start()`: initialize own's NuttX internal structures), and
 3. Application Initialization (`user_start()`).
 
This initialization sequence is really quite simple because the system runs in single-thread mode up until the point that is starts the application. That means that the initialization sequence is just simple, straight-line function calls.

Just before starting the application, the system goes to  multi-threaded mode and things can get more complex.

Each of these will be discussed in more detail in the following paragraphs.

> **TODO**: add overview of call order of common functions before going to architecture specific examples (os_start, etc.)

## Power-On Reset Initialization.

The software begins execution when the processor is reset. This usually at power-on, but all resets are basically the same where they occur because of power-on, pressing the reset button, or on a watchdog timer expiration.

The logic that executes when the processor is reset is unique to the particular CPU architecture and is not a common part of NuttX. The kinds of things that must be done by the architecture-specific reset handling includes:

 1. Putting the processor in its operational state. This may include things like setting CPU modes; initializing co-processors, etc.
 2. Setting up clocking so that the software and peripherals operate as expected,
 3. Setting up the C stack pointer (and other processor registers)
 4. Initializing memory, and
 5. Starting NuttX.
 
### Memory Initialization

In C implementations, there are two general classes of variable storage. First there are the initialized variables. 

For example, consider the global variable `x`:

    int x = 5;
    
The C code must be assured that after reset, the variable x has the value 5. Initialized variable of this kind are retained in a special memory section called *data* (or `.data`).

Other variables are not initialized. Like the global variable y:

    int y;
    
But the C code will still expect `y` to have an initial value. That initial value will be zero. All uninitialized variables of this this type have have the value zero. These uninitialized variables are retained in a section called *bss* (or `.bss`).

When we say that the reset handling logic initializes memory, we mean two things:

 1. It provides the (initial) values of the initialized variables by copying the values from FLASH into the `.data` section, and
 2. It resets all of the uninitialized variables to zero. It clears the `.bss` section.
 
### The `__start` call

Since the definition of this function is architecture-specific lets walk through reset sequence of one particular processor. Let's look at the NuttX initialization for
the STM32 F4 MCU. This reset logic can be found in to files:

    nuttx/arch/arm/src/stm32_vectors.S
    nuttx/arch/arm/src/stm32_start.c

#### Vectors File

The roll of `stm32_vectors.S` in this reset sequence is very small. This file provides all of the STM32 exception vectors and power-on reset is simply another exception vector. Some important things to note about this file.

First, you will notice the instruction `.section .vectors, "ax"`. This pseudo operation will place all of the vectors into a special section call `.vectors`. One of the STM32 F4 linker scripts is located at
`nuttx/configs/stm3240g-eval/nsh/ld.script`. In that file, you can see that the section `.vectors` is forced to lie at the very beginning of FLASH memory. The STM32 F4 can be configured to boot in different ways via strapping. If it is
strapped to boot from FLASH, then the STM32 FLASH memory will be aliased to
address `0x0000 0000` when the reset occurs. The is the address of the power-up reset interrupt vector.

Second, the first two 32-bit entries in the vector table represent the power-up exception vector (which we know will be positioned at address `0x0000 0000` when the reset occurs). Those two entries are:

    .word IDLE_STACK /* Vector 0: Reset stack pointer */
    .word __start /* Vector 1: Reset vector */
    
The Cortex-M family is unique in the way that is handles the reset vector. Notice that there are two values: the stack pointer for the start-up thread (the IDLE thread), and the entry point in the IDLE thread. When the reset occurs, the the stack pointer is automatically set to the first value
and then the processor jumps to reset entry point `__start` specified in the second entry. This means that the reset exception handling code can be implemented in C rather than assembly language.

#### Start File

The reset vector `__start` lies in the file `stm32_start.c` and does the real, low-level architecture-specific initialization. This initialization includes:
 * `stm32_clockconfig()`: Initialize the PLLs and peripheral clocking needed by the board.
 * `stm32_fpuconfig()`: If the STM32 F4's hardware floating point is initialized, then configure the FPU and enable access to the FPU co-processors.
 * `stm32_lowsetup()` : Enable the low-level UART. This is done very early in initialization so that we can get serial debug output to the console as soon as possible. If you are doing a board bring-up this is very important.
 * `stm32_gpioinit()` : Perform any GPIO remapping that is needed (this is a stub for the F4, but the F1 family requires this step).
 * `showprogress('A')` This simply outputs the character 'A' on the serial console (only if `CONFIG_DEBUG` is enabled). If debug is enabled, you will always see the letters ABDE output on the console. That output all comes from this file.
 
Next the memory is initialized:
 * The `.bss` section is set to zero (Letter 'B' is then output if `CONFIG_DEBUG` is enabled), then
 * The `.data` section is set to its initial values (The letter 'C' is output if debug is enabled). Then board-specific logic is initialized:
 * `stm32_boardinitialize()` This function resides with the board-specific logic.
 
For the case of the STM3240G-EVAL board, the `stm32_boardinitialize()` does the following
operations:
 * `stm32_spiinitialize()` Initialize SPI chip selects if SPI is enabled.
 * `stm32_selectsram()` Configure the STM32 FSMC to support external SRAM if external SRAM support is enabled.
 * `up_ledinit()` Initialize the on-board LEDs if they are used.
 
When `stm32_boardinitialize()` returns to `__start()`, the low-level, architecture specific initialization is complete and NuttX is started:
 * `os_start()` This is the NuttX entry point. It performs the next phase of RTOS specific initialization and then brings up the application. The operations performed by `os_start` are discussed in the next paragraph

## NuttX RTOS Initialization

When the low-level, architecture-specific initialization is complete and NuttX is started by calling the
function `os_start()`. This function resides in the file `nuttx/sched/os_start.c`. The operations performed by `os_start()` are summarized below. Note that many of these features can be disabled from the NuttX configuration file and in that case those operations are not performed:

 1. Initializes some NuttX global data structures,
 2. Initializes the TCB for the IDLE thread (i.e, the thread that the initialization is performed on),
 3. `kmm_initialize()` Initialize the memory manager (in most configurations, `kmm_initialize()` is an alias for the common `mm_initialize()`).
 4. `irq_initialize()` Initialize the interrupt handler subsystem. This initializes only data
structures; CPU interrupts are still disabled.
 5. `wd_initialize()` Initialize the NuttX watchdog timer facility,
 6. `clock_initialize()` Initialize the system clock,
 7. `timer_initialize()` Initialize the POSIX timer facilities,
 8. `sig_initialize()` Initialize the POSIX signal facilities,
 9. `sem_initialize()` Initialize the POSIX semaphore facilities,
 10. `mq_initialize()` Initialize the POSIX message queue facilities,
 11. `pthread_initialize()` Initialize the POSIX pthread facilities,
 12. `fs_initialize()` Initialize file system facilities,
 13. `net_initialize()` Initialize networking facilities. 

Up to this point, all of the initialization steps have only been software initializations. Nothing has
interacted with the hardware. Rather, all of these steps simply prepared the environment so that things
like interrupts and threads can function properly. The next phases depend upon that setup.

 1. `up_initialize()` The processor specific details of running the operating system will be handled here. Such things as setting up interrupt service routines and starting the clock are some of the things that are different for each processor and hardware platform. See below for a specific example of the initialization steps performed by the ARM version of this function. Then,
 2. `lib_initialize()` Initialize the C libraries. This is done last because the libraries may depend on the above.
 3. `sched_setupidlefiles()` This is the logic that opens `/dev/console` and creates `stdin`, `stdout`, and `stderr` for the IDLE thread. All tasks subsequently created by the
IDLE thread will inherit these file descriptors.
 4. `os_bringup()` Create the initial tasks. This will be described more below.
 5. And finally enter the IDLE loop.
 
After completing the initialization, the roll of the IDLE thread changes. It is now becomes the thread that executes only when there is nothing else to do in the system (hence, the name IDLE thread).

### `os_bringup()`

This function is called at the very end of the initialization sequence in `os_start()`, just before
entering the IDLE loop, and starts all of the required threads and tasks needed to bring up the system. This function performed the following specific operations:

 1. If on-demand paging is configured, this function will start the page fill task. This is the task that runs in order to satisfy page faults in processors that have an MMU and in configurations where on-demand paging is enabled.
 2. Then, if so configured, this function starts the worker thread. This thread may be used to execute any processing deferred to the worker thread via a specific API. The worker thread's primary function is intended for extended device driver processing but can be used for a variety of purposes.
 3. Finally, `os_bringup()` will start the initial application task. By default this is the task whose entry
has the name `user_start()` which is provided by application code and when it runs, it begins the application-specific phase of the initialization sequence as described below.

### IDLE Thread Activities.

As previously mentioned, the IDLE thread is the thread that executes only when there is nothing else to do in the
system (typically putting the CPU to sleep until the
next interrupt occurs). It always has the lowest priority in the system (priority 0), it is the only thread that is
permitted to have this level and it can never be blocked (otherwise, what would then run?).

As a result, the IDLE thread is always in the *read to run* list and, in fact, since that list is prioritized, can guaranteed to always be the final entry of this list.

The IDLE thread is an an infinite loop, however this does not make it a *CPU hog*. Since it is the lowest priority,
it can be suspended whenever anything else needs to run.

The IDLE thread does two things in this infinite loop. First, the IDLE thread will perform memory clean-up. Memory clean is required to handle deferred memory deallocation. Memory allocations must be deferred when the memory is freed in a context where the software does not
have access to the heap and, hence, cannot truly free the memory (such as in an interrupt handler). In this case, the memory is simply put into a list of freed memory and, eventually, cleaned up by the IDLE thread.

The second step of the IDLE thread is to call `up_idle()`. The operations performed by `up_idle()` are architecture- and board-specific. In general, this is the location where CPU-specific reduced power operations may be performed.

### Example: ARM architecture `up_initialize()`

All ARM-based MCUs share a common `up_initialize()` implementation. The operations performed by this common ARM
initialization will, however, call into facilities provided by the particular ARM chip. The common ARM initialization sequence is:
 1. `up_calibratedelay()`; One operation that must be performed during a CPU port is the calibration of timing delay loops. If `CONFIG_ARCH_CALIBRATION` is defined, then
`up_initialize()` will perform some specific operations for the calibration of the delay loop. This, however, is not part of the normal initialization sequence.
 2. `up_addregion()` The basic heap was set up during processing by `os_start()`. However, if the board supports multiple, discontiguous memory regions, any additional memory regions can by added to the heap by this function. 
 3. `up_irqinitialize()` This function initializes the interrupt subsystem.
 4. `up_pminitialize()` If `CONFIG_PM` is defined, the function must initialize the power management subsystem. This MCU-specific function must be called very early in the
initialization sequence before any other device drivers are initialized (since they may attempt to register with the power management subsystem).
 5. `up_dmainitialize()` Initialize the DMA subsystem.
 6. `up_timerinit()` Initialize the system timer interrupt.
 7. `devnull_register()` Registers the standard `/dev/null`.
 8. Then this function initializes the console device (if any). This means calling one of (1) `up_serialinit()` for the standard serial driver, (2) `lowconsole_init()` for the low-level, write-only serial console, or (3) `ramlog_sysloginit()` for the RAM console.
 9. `up_netinitialize()` Initialize the network. 
 10. `up_usbinitialize()` Initialize USB (host or device).
 11. `up_ledon(LED_IRQSENABLED)`. Finally, up_initialize() illuminates board-specific LEDs to indicate the IRQs are now enabled.
 
## Application Initialization

At the conclusion of the OS initialization phase in `os_start()`, the selected user application is started by creating a new task at the entry point `user_start()`. There must be exactly one entry point called `user_start()` in every application built on top of NuttX. Any additional initialization performed in the `user_start()` function is purely application dependent.

> **TODO** is this name still correct? 

### A Simple Hello World Application
The simplest user application would be the “Hello, World!” example. See `apps/examples/hello`. Here is the whole example:

```
int user_start(int argc, char *argv[])
{
 printf("Hello, World!!\n");
 return 0;
}
```

In this case, no additional application initialization is needed. It just “says hello” and exits.